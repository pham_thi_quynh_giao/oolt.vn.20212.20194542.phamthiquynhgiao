package hust.soict.hedspi.gui.awt;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AwtCounter extends Frame implements ActionListener{
	private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count = 0;
    
    public AwtCounter() {
    	//bo cuc dang don gian
    	this.setLayout(new FlowLayout());
    	//khoi tao cac component va them vao giao dien
    	lblCount = new Label("Counter");
    	this.add(lblCount);
    	
    	tfCount = new TextField(count + "",10);
    	tfCount.setEditable(false);
    	add(tfCount);
    	
    	btnCount = new Button("Count");
    	add(btnCount);
//Xu li su kien 
//C1: tao lop thuc thi giao dien listener rieng biet(it su dung)
//C2: Khai bao cho frame thuc thi giao dien cua interface(thg dung)
//C3: Khai bao lop vo danh
    	//Dang ki lang nghe su kien tren button
//    	btnCount.addActionListener( new ActionListener(){
//    		public void actionPerforment(ActionEvent e) {
//    			++count;
//     			tfCount.setText(count + "");
//    		}
//    		
//    	}
//		);
    	this.setTitle("ATM Counter");
    	this.setSize(250, 100);
    	this.setVisible(true);
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AwtCounter app = new AwtCounter();        
        
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		++count;
		tfCount.setText(count + "");
	}

}
